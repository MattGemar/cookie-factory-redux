import actionTypes from '../actions/actionTypes';

const initialState = {
	cookies: 0,
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.CREATE_COOKIE:
			return {
				cookies: state.cookies + 1,
			};
		case actionTypes.EAT_COOKIE:
			return {
				cookies: state.cookies - 1,
			};
		case actionTypes.CREATE_COOKIE_BOX:
			return {
				cookies: state.cookies + action.value,
			};
		case actionTypes.EAT_COOKIE_BOX:
			return {
				cookies: state.cookies - action.value,
			};
		case actionTypes.AUTO_COOKIE:
			return {
				cookies: state.cookies + action.value,
			};
		default:
			return state;
	}
};

export default reducer;
