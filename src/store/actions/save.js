import actionTypes from './actionTypes';

export const saveCookies = (value) => {
	return {
		type: actionTypes.SAVE_COOKIES,
		value: value,
	};
};
