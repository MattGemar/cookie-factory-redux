import actionTypes from './actionTypes';

export const createCookie = () => {
	return {
		type: actionTypes.CREATE_COOKIE,
	};
};

export const eatCookie = () => {
	return {
		type: actionTypes.EAT_COOKIE,
	};
};

export const createCookieBox = (value) => {
	return {
		type: actionTypes.CREATE_COOKIE_BOX,
		value: value,
	};
};

export const eatCookieBox = (value) => {
	return {
		type: actionTypes.EAT_COOKIE_BOX,
		value: value,
	};
};

const autoCookieAsync = (cookies) => {
	return {
		type: actionTypes.AUTO_COOKIE,
		value: cookies + 1,
	};
};

export const autoCookie = (value) => {
	return (dispatch) => {
		setInterval(() => {
			dispatch(autoCookieAsync(value));
		}, 4000);
	};
};
