// Librairies
import React from 'react';
import classes from './Fabriquer.module.css';
import * as actionCreators from '../../store/actions/index';

// Redux
import { connect } from 'react-redux';

function Fabriquer(props) {
	return (
		<div className={classes.Fabriquer}>
			<button onClick={props.handleCreateCookie}>Fabriquer un cookie</button>
			<button onClick={props.handleEatCookie}>Manger un cookie</button>
			<button
				onClick={() => {
					props.handleCreateCookieBox(5);
				}}>
				Fabriquer une boite de cookies
			</button>
			<button
				onClick={() => {
					props.handleEatCookieBox(5);
				}}>
				Manger une boite entière de cookies
			</button>
			<button
				onClick={() => {
					props.handleSaveCookies(props.cookies);
				}}>
				Cacher la reserve de cookies
			</button>
		</div>
	);
}

// Récupérer les actions
const mapDispatchToProps = (dispatch) => {
	return {
		handleCreateCookie: () => dispatch(actionCreators.createCookie()),
		handleEatCookie: () => dispatch(actionCreators.eatCookie()),
		handleCreateCookieBox: (value) =>
			dispatch(actionCreators.createCookieBox(value)),
		handleEatCookieBox: (value) => dispatch(actionCreators.eatCookieBox(value)),
		handleSaveCookies: (value) => dispatch(actionCreators.saveCookies(value)),
	};
};

// Abonnement au state
const mapStateToProps = (state) => {
	return {
		cookies: state.cookie.cookies,
	};
};
export default connect(mapStateToProps, mapDispatchToProps)(Fabriquer);
