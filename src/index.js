// Librairies
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';

// Composants
import App from './App';

// Redux
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import cookiesReducer from './store/reducers/cookies';
import saveCookiesReducer from './store/reducers/save';
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const reducer = combineReducers({
	cookie: cookiesReducer,
	saveCookies: saveCookiesReducer,
});

// Création du middleware
const logger = (store) => {
	return (next) => {
		return (action) => {
			console.log(store.getState());
			console.log(action);
			return next(action);
		};
	};
};

const store = createStore(
	reducer,
	composeEnhancers(applyMiddleware(logger, thunk))
);

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<App />
		</Provider>
	</React.StrictMode>,
	document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
