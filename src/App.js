// Librairies
import React, { useEffect } from 'react';
import classes from './App.module.css';
import * as actionCreators from './store/actions/index';

// Composants
import Header from './Components/Header/Header';
import Fabriquer from './Components/Fabriquer/Fabriquer';

// Redux
import { connect } from 'react-redux';

function App(props) {
	useEffect(() => {
		props.handleAutoCookie(props.cookies);
	}, []);

	let history;

	if (props.history && props.history !== '') {
		history = props.history.map((result) => (
			<div key={result.id} className={classes.result}>
				<span>
					<b>{result.value}</b> cookies stockés.
				</span>
				Le {new Date(result.id).toLocaleString('fr-FR')}
			</div>
		));
	}

	return (
		<div className={classes.App}>
			<Header />

			<div className='container'>
				<div className={classes.content}>
					<h1>Mangez les tous!</h1>
					<div className={classes.cookies}>
						<span>{props.cookies}</span>
						cookies fabriqués
					</div>
				</div>

				<Fabriquer />
				{props.history && props.history !== '' ? (
					<div className={classes.content}>
						<h2> Tableau des stocks </h2>
						{history}
					</div>
				) : null}
			</div>
		</div>
	);
}

// Récupérer les actions
const mapDispatchToProps = (dispatch) => {
	return {
		handleAutoCookie: (value) => dispatch(actionCreators.autoCookie(value)),
	};
};

// Abonnement au state
const mapStateToProps = (state) => {
	return {
		cookies: state.cookie.cookies,
		history: state.saveCookies.history,
	};
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
